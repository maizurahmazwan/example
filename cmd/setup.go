package main

import (
	"context"
	"html/template"
	"io"
	"net/http"
	"pbo-server/ui"
	"strconv"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/labstack/echo/v4"
	"github.com/ziflex/lecho"
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4/middleware"
)

type Template struct {
	Templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.Templates.ExecuteTemplate(w, name, data)
}

var (
	// configure the logger's behaviour here
	logsConfig = logs.ConfigSet{}
	logger     = logs.Configure(logsConfig)
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.validator.Struct(i); err != nil {
		// Optionally, you could return the error to give each route more control over the status code
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return nil
}

func setupEcho() *echo.Echo {

	e := echo.New()

	// Setup Echo to use our logger
	e.Logger = lecho.New(logger) // Echo adapter for Zerolog

	e.Validator = &CustomValidator{validator: validator.New()}

	// prepare our templates for Echo's renderer
	t := &Template{
		// read template files from our embedded ui.WebContent FS(filesystem) with
		// ParseFS instead of reading from external sources with ParseGlob
		// in this way, everything we need is in a single executable(binary)
		Templates: template.Must(template.ParseFS(ui.WebContent, "templates/*.tmpl")),
	}
	e.Renderer = t

	// Setup Echo's middleware
	e.Use(middleware.TimeoutWithConfig(middleware.TimeoutConfig{
		Timeout: 5 * time.Second,
	}))

	// log every request
	e.Use(middleware.Logger())
	e.Use(middleware.RequestID())
	e.Use(middleware.CORS())

	return e
}

func InitMDMDatabase() *pgxpool.Pool {
	dbPort, err := strconv.ParseInt(env.Get("DB_PORT"), 0, 16)

	if err != nil {
		logs.Fatal().Err(err).Msg("unable to establish MDM database connection")
	}

	dbPool := database.InitDatabase(env.Get("DB_USER"), env.Get("DB_PASSWORD"), env.Get("DB_SERVER"), env.Get("DB_NAME"), uint16(dbPort), logger)

	err = dbPool.Ping(context.Background())

	if err != nil {
		logs.Fatal().Err(err).Msg("db pool ping error")
	}

	logs.Info().Msg("Established database connection to : " + env.Get("DB_NAME") + ":" + env.Get("DB_SERVER"))
	return dbPool
}

func InitAXISDatabase() *pgxpool.Pool {
	axisDBPort, err := strconv.ParseInt(env.Get("AXIS_DATABASE_PORT"), 0, 16)

	if err != nil {
		logs.Fatal().Err(err).Msg("unable to establish AXIS database connection")
	}

	axisDBPool := database.InitDatabase(env.Get("AXIS_DATABASE_USERNAME"), env.Get("AXIS_DATABASE_PASSWORD"), env.Get("AXIS_DATABASE_SERVER"), env.Get("AXIS_DATABASE_NAME"), uint16(axisDBPort), logger)

	err = axisDBPool.Ping(context.Background())

	if err != nil {
		logs.Fatal().Err(err).Msg("db pool ping error")
	}
	logs.Info().Msg("Established database connection to : " + env.Get("AXIS_DATABASE_NAME") + ":" + env.Get("AXIS_DATABASE_SERVER"))
	return axisDBPool
}
