package main

import (
	"context"

	"net/http"
	"time"

	"pbo-server/internal/routes"
	"pbo-server/pkg/dbconnections"
	"pbo-server/pkg/utilities"

	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/logs"
)

var (
	echoPortNumber = "1234"
)

// @title Back Office
// @version 1.0
// @description This is the POS Back Office

// @contact.name POS BackOffice Developer
// @contact.email boojiun@pos.com.my

// @host localhost:1234
// @BasePath /
func main() {
	// setup Echo to use our golib/logs
	e := setupEcho()

	// Initialize routes.
	routes.InitRoutes(e)

	// Init MDM database connection
	dbconnections.MDMDBPool = InitMDMDatabase()

	// Init AXIS database connection
	dbconnections.AXISConnectionPool = InitAXISDatabase()

	// Start server by spinning a goroutine so that it will become non-blocking
	go func() {
		if err := e.Start(":" + echoPortNumber); err != nil && err != http.ErrServerClosed {
			logs.Fatal().Msg("shutting down the server")
		}
	}()

	// Gracefully shutdown database, etc services before shutting down Echo server
	// with 10 seconds timeout
	utilities.Graceful(utilities.StopWaitWrapper(func(ctx context.Context) {

		database.WrapClose(dbconnections.MDMDBPool)
		database.WrapClose(dbconnections.AXISConnectionPool)
		e.Shutdown(ctx)
		logs.Info().Msg("Backoffice server shutdown")
		logs.Close()

	}, 10*time.Second))
}
