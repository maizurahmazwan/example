package utilities

import (
	"fmt"

	"github.com/golang-jwt/jwt"
	"gitlab.com/pos_malaysia/golib/env"
)

// ExtractToken returns the token from a given token string.
func ExtractToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(env.Get("JWT_SECRET")), nil
	})
	return token, err
}
