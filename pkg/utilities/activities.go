package utilities

import (
	"pbo-server/pkg/dbconnections"
	"pbo-server/pkg/http/responses"

	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/logs"
)

// Insert into activities table to track user activity
func InsertActivity(c echo.Context, action string, by_who string, table_name string) {
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	_, err := database.WrapExec(dbconnections.MDMDBPool, c.Request().Context(), "InsertIntoActivities", action, by_who, table_name)

	if err != nil {
		loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()
		loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapExec error")
	}
}
