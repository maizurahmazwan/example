package dbconnections

import "github.com/jackc/pgx/v4/pgxpool"

var (
	MDMDBPool          *pgxpool.Pool
	AXISConnectionPool *pgxpool.Pool
)
