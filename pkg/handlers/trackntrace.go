package handlers

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"io"
	"net/http"
	"pbo-server/internal/models"
	"pbo-server/pkg/http/responses"
	"pbo-server/pkg/utilities"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/contextkeys"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"
)

var templateDataMap = make(map[string]interface{})

func TTU(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "trackntrace", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}

func UploadTTU(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	if c.Request().Method == "POST" {
		file, _, err := c.Request().FormFile("csvFile")

		//file, header, err := c.Request().FormFile("csvFile")
		//fmt.Println(file)
		//fmt.Println(header)

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting uploaded file")
			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "trackntrace", templateDataMap)

		}

		// validate the uploaded file
		reader := csv.NewReader(file)
		reader.Comma = '|'
		reader.Comment = '#'
		reader.LazyQuotes = true
		records, err := reader.ReadAll()
		if len(records) == 0 || err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error opening uploaded csv file")

			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "trackntrace", templateDataMap)

		}

		// sanity check on each column
		//fmt.Println(records)
		const Cols = 4
		for _, record := range records {
			if len(record) != Cols {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in csv content")

				DataMap["UploadError"] = true
				DataMap["Message"] = "number of columns in csv doesn't match, expected " + strconv.Itoa(Cols)
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "trackntrace", templateDataMap)
			}

			data := models.Copywriting{
				ID:     record[0],
				Msg:    record[1],
				Status: record[2],
				Title:  record[3],
			}

			if data.ID == "" {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in csv content")

				DataMap["UploadError"] = true
				DataMap["Message"] = err.Error()
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "trackntrace", templateDataMap)
			}
		}

		// upload to S3 bucket
		cfg, err := config.LoadDefaultConfig(ctx, config.WithRegion(env.Get("AWS_S3_REGION")))
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("AWS S3 config.LoadDefaultConfig error")
			return echo.ErrInternalServerError

		}

		awsS3Client := s3.NewFromConfig(cfg)

		// key to the file to be uploaded
		var key string = "data/copywriting.csv"

		// to fix issue in aws-sdk-go, see
		// https://github.com/aws/aws-sdk-go/issues/1962#issuecomment-406783345
		if _, err := file.Seek(0, io.SeekStart); err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("file seek start failed")
			return echo.ErrInternalServerError
		}

		uploader := manager.NewUploader(awsS3Client)
		result, err := uploader.Upload(ctx, &s3.PutObjectInput{
			Bucket:               aws.String(env.Get("AWS_S3_BUCKET")),
			Key:                  aws.String(key),
			ContentType:          aws.String("text/plain"),
			ServerSideEncryption: types.ServerSideEncryptionAes256,
			Body:                 file,
		})

		//fmt.Println(result)

		if err != nil || result.ETag == nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("s3 uploader.Upload error : " + err.Error())
			return echo.ErrInternalServerError
		}

		// log activity
		utilities.InsertActivity(c, "UploadTTU: UploadToS3", DataMap["Username"].(string), "data/copywriting.csv")

		// trigger the refresh API to propagate the latest copywriting.csv file
		// content to all the TTU's pods.
		resp, err := http.Get(env.Get("TTU_REFRESH"))
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("trigger refresh API error")

			// proceed even if fail to trigger refresh API for immediate update.
			// the cron scheduler inside each TTU pods will auto refresh later.

		}

		defer resp.Body.Close()

		var refreshResponseBody bytes.Buffer
		_, err = io.Copy(&refreshResponseBody, resp.Body)
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting response from pocket base.")
			return echo.ErrInternalServerError
		}

		var refresh models.RefreshResponse
		err = json.Unmarshal(refreshResponseBody.Bytes(), &refresh)

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting response from pocket base.")
			return echo.ErrInternalServerError
		}

		DataMap["UploadSuccess"] = true
		DataMap["Message"] = refresh.Message
		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "trackntrace", templateDataMap)

	}

	// user did not select a file and click submit button.
	return echo.ErrBadRequest
	//return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH")+"/ttu")
}
