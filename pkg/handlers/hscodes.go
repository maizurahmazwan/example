package handlers

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"pbo-server/internal/models"
	"pbo-server/pkg/dbconnections"
	"pbo-server/pkg/http/responses"
	"pbo-server/pkg/utilities"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/jackc/pgx/v4"
	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/contextkeys"
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"
)

func Hscodes(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "hscodes", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}

func UploadHscodes(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	if c.Request().Method == "POST" {
		file, _, err := c.Request().FormFile("csvFile")

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting uploaded file")
			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "hscodes", templateDataMap)

		}

		// validate the uploaded file
		reader := csv.NewReader(file)
		reader.Comma = ','
		reader.Comment = '#'
		reader.LazyQuotes = true
		recordsFromCSV, err := reader.ReadAll()
		if len(recordsFromCSV) == 0 || err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error opening uploaded csv file")

			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "hscodes", templateDataMap)

		}

		// remove first row if it's header
		if strings.EqualFold(recordsFromCSV[0][0], "hscode") && strings.EqualFold(recordsFromCSV[0][1], "keyword") {
			recordsFromCSV = recordsFromCSV[1:]
		}

		// sanity check on each column
		for _, record := range recordsFromCSV {
			data := models.HScodes{
				HScode:  record[0],
				Keyword: record[1],
			}

			// make sure all fields are not empty
			if data.HScode == "" || data.Keyword == "" {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in csv content")

				DataMap["UploadError"] = true
				DataMap["Message"] = err.Error()
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "hscodes", templateDataMap)
			}

		}

		// load all rows from database
		var hsCodesFromDB []models.HScodes

		errDB := database.WrapSelect(dbconnections.MDMDBPool, ctx, &hsCodesFromDB, "GetAllFromHSCodes")
		if errDB != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapSelect error")
			DataMap["UploadError"] = true
			DataMap["Message"] = errDB
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "hscodes", templateDataMap)
		}

		// insert - find additional row inside csv
		var insertFromCSV []models.HScodes

		for _, record := range recordsFromCSV {
			data := models.HScodes{
				HScode:  strings.TrimSpace(record[0]),
				Keyword: strings.TrimSpace(record[1]),
			}

			if !strings.EqualFold(data.HScode, "hscode") && !strings.EqualFold(data.Keyword, "keyword") {
				insertFromCSV = append(insertFromCSV, data)
			}

		}

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true
		DataMap["InsertCodes"] = insertFromCSV
		DataMap["ExitingRowsCount"] = len(hsCodesFromDB)
		DataMap["NewRowsCount"] = len(insertFromCSV)

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "updatehscodes", templateDataMap)

	}

	// user did not select a file and click submit button.
	return echo.ErrBadRequest
	//return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH")+"/hscodes")
}

func UpdateHscodes(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)

	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		err := c.Request().ParseForm()

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error parsing form")
		}

	}

	if c.Request().Method == "POST" {

		var dataToInsert []models.HScodes
		var data models.HScodes

		for formKey, inputValues := range c.Request().Form {
			if formKey == "hsCodesValue[]" {

				for _, value := range inputValues {
					row := strings.Split(value, ",")

					data.HScode = row[0]
					data.Keyword = row[1]

					dataToInsert = append(dataToInsert, data)
				}
			}
		}

		_, err := database.WrapExec(dbconnections.MDMDBPool, ctx, "DeleteAllFromHSCodes")

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error in deleting all in hscodes table")

			DataMap["UploadSuccess"] = false
			DataMap["Message"] = "unable to delete all records in hscodes table."
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "hscodes", templateDataMap)

		} else {
			// log activity
			utilities.InsertActivity(c, "UpdateHscodes: DeleteAllFromHSCodes", DataMap["Username"].(string), "hscodes")

			// proceed with insert after successful delete
			rowsToInsert := [][]interface{}{}

			for i := 0; i < len(dataToInsert); i++ {
				row := []interface{}{dataToInsert[i].HScode, dataToInsert[i].Keyword}
				rowsToInsert = append(rowsToInsert, row)
			}

			copyCount, err := database.WrapCopyFrom(dbconnections.MDMDBPool, ctx, pgx.Identifier{"hscodes"},
				[]string{"hscode", "keyword"},
				pgx.CopyFromRows(rowsToInsert))

			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in insert new records to hscodes table")
				DataMap["UploadSuccess"] = false
				DataMap["Message"] = err.Error()
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "hscodes", templateDataMap)
			}

			if int(copyCount) != len(dataToInsert) {
				loggerWithTrace.Info().Msgf("Expected CopyFrom to return %d copied rows, but got %d", len(rowsToInsert), copyCount)

				DataMap["UploadSuccess"] = false
				DataMap["Message"] = "The number rows to insert not equal to the number of copied rows."
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "hscodes", templateDataMap)
			} else {
				// log activity
				utilities.InsertActivity(c, "UpdateHscodes: InsertAllIntoHSCodes", DataMap["Username"].(string), "hscodes")

				copyCountString := fmt.Sprintf("%d", copyCount)
				DataMap["Message"] = copyCountString + " new records inserted."
				DataMap["UploadSuccess"] = true
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "hscodes", templateDataMap)
			}

		}

	}

	// user token has expired, return the user back to home page
	return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))

}
