package handlers

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"pbo-server/internal/models"
	"pbo-server/pkg/dbconnections"
	"pbo-server/pkg/http/responses"
	"pbo-server/pkg/utilities"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/jackc/pgx/v4"
	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/contextkeys"
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"
)

func DialingCodes(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "dialingcodes", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}

func UploadDialingCodes(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	if c.Request().Method == "POST" {
		file, _, err := c.Request().FormFile("csvFile")

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting uploaded file")
			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "dialingcodes", templateDataMap)

		}

		// validate the uploaded file
		reader := csv.NewReader(file)
		reader.Comma = ','
		reader.Comment = '#'
		reader.LazyQuotes = true
		recordsFromCSV, err := reader.ReadAll()
		if len(recordsFromCSV) == 0 || err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error opening uploaded csv file")

			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "dialingcodes", templateDataMap)

		}

		// remove first row if it's header
		if strings.EqualFold(recordsFromCSV[0][0], "country") && strings.EqualFold(recordsFromCSV[0][1], "code") && strings.EqualFold(recordsFromCSV[0][1], "calling_code") {
			recordsFromCSV = recordsFromCSV[1:]
		}

		// sanity check on each column
		for _, record := range recordsFromCSV {
			data := models.DialingCodes{
				Country:     record[0],
				Code:        record[1],
				CallingCode: record[2],
			}

			// make sure all fields are not empty
			if data.Code == "" || data.Country == "" || data.CallingCode == "" {
				loggerWithTrace.Error().Caller().Msg("error in csv content")

				DataMap["UploadError"] = true
				DataMap["Message"] = "Country, Code or CallingCode cannot be empty string"
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "dialingcodes", templateDataMap)
			}

		}

		// load all rows from database
		var dialingCodesFromDB []models.DialingCodes

		errDB := database.WrapSelect(dbconnections.MDMDBPool, ctx, &dialingCodesFromDB, "GetAllFromDialingCodes")
		if errDB != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapSelect error")
			DataMap["UploadError"] = true
			DataMap["Message"] = errDB
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "dialingcodes", templateDataMap)
		}

		// insert - find additional row inside csv
		var insertFromCSV []models.DialingCodes

		for _, record := range recordsFromCSV {
			data := models.DialingCodes{
				Code:        strings.TrimSpace(record[0]),
				Country:     strings.TrimSpace(record[1]),
				CallingCode: strings.TrimSpace(record[2]),
			}

			if !strings.EqualFold(data.Code, "code") && !strings.EqualFold(data.Country, "country") && !strings.EqualFold(data.CallingCode, "calling_code") {
				insertFromCSV = append(insertFromCSV, data)
			}

		}

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true
		DataMap["InsertCodes"] = insertFromCSV
		DataMap["ExitingRowsCount"] = len(dialingCodesFromDB)
		DataMap["NewRowsCount"] = len(insertFromCSV)

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "updatedialingcodes", templateDataMap)

	}

	// user did not select a file and click submit button.
	return echo.ErrBadRequest
	//return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH")+"/dialingcodes")
}

func UpdateDialingCodes(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)

	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		err := c.Request().ParseForm()

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error parsing form")
		}

	}

	if c.Request().Method == "POST" {

		var dataToInsert []models.DialingCodes
		var data models.DialingCodes

		for formKey, inputValues := range c.Request().Form {
			if formKey == "dialingCodesValue[]" {

				for _, value := range inputValues {
					row := strings.Split(value, "|")

					data.Country = row[0]
					data.Code = row[1]
					data.CallingCode = row[2]

					dataToInsert = append(dataToInsert, data)
				}
			}
		}

		_, err := database.WrapExec(dbconnections.MDMDBPool, ctx, "DeleteAllFromDialingCodes")

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error in deleting all in countries_dialing_codes table")

			DataMap["UploadSuccess"] = false
			DataMap["Message"] = "unable to delete all records in countries_dialing_codes table."
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "dialingcodes", templateDataMap)

		} else {
			// log activity
			utilities.InsertActivity(c, "UpdateDialingCodes: DeleteAllFromDialingCodes", DataMap["Username"].(string), "countries_dialing_codes")

			// proceed with insert after successful delete
			rowsToInsert := [][]interface{}{}

			for i := 0; i < len(dataToInsert); i++ {
				row := []interface{}{dataToInsert[i].Code, dataToInsert[i].Country, dataToInsert[i].CallingCode}
				rowsToInsert = append(rowsToInsert, row)
			}

			copyCount, err := database.WrapCopyFrom(dbconnections.MDMDBPool, ctx, pgx.Identifier{"countries_dialing_codes"},
				[]string{"code", "country", "calling_code"},
				pgx.CopyFromRows(rowsToInsert))

			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in insert new records to countries_dialing_codes table")
				DataMap["UploadSuccess"] = false
				DataMap["Message"] = err.Error()
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "dialingcodes", templateDataMap)
			}

			if int(copyCount) != len(dataToInsert) {
				loggerWithTrace.Info().Msgf("Expected CopyFrom to return %d copied rows, but got %d", len(rowsToInsert), copyCount)

				DataMap["UploadSuccess"] = false
				DataMap["Message"] = "The number rows to insert not equal to the number of copied rows."
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "dialingcodes", templateDataMap)
			} else {
				// log activity
				utilities.InsertActivity(c, "UpdateDialingCodes: InsertAllIntoDialingCodes", DataMap["Username"].(string), "countries_dialing_codes")

				copyCountString := fmt.Sprintf("%d", copyCount)
				DataMap["Message"] = copyCountString + " new records inserted."
				DataMap["UploadSuccess"] = true
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "dialingcodes", templateDataMap)
			}

		}

	}

	// user token has expired, return the user back to home page
	return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))

}
