package handlers

import (
	"net/http"
	"pbo-server/internal/models"
	"pbo-server/pkg/dbconnections"
	"pbo-server/pkg/http/responses"
	"pbo-server/pkg/utilities"
	"strconv"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/contextkeys"
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"
)

func AxisTransformationEvents(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	// page from path /transformationevents/:page
	pageString := c.Param("page")

	page := int64(0)
	if pageString != "" {
		pageInt, err := strconv.Atoi(pageString)
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error converting pageString to integer")
			// instead of return unauthorized error, return the user back to home page
			return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
		}
		page = int64(pageInt)
	}

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		// extract data from t_axis_transformation_events table
		var AxisTransformationEventsFromDB = []models.AxisTransformationEventFromDB{}

		var AxisTransformationEvents = []models.AxisTransformationEvent{}

		limit := int64(10)
		offset := limit * (page - 1)

		err := database.WrapSelect(dbconnections.AXISConnectionPool, ctx, &AxisTransformationEventsFromDB, "GetAxisTransformationEvents", offset, limit)
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapSelect error")
		}

		// these values from database can be NULL
		var headers, payload, HTTPStatusCode string

		// need to process first
		for _, event := range AxisTransformationEventsFromDB {

			if event.Headers != nil {
				headers = *event.Headers
			}

			if event.Payload != nil {
				payload = *event.Payload
			}

			if event.HTTPStatusCode != nil {
				HTTPStatusCode = *event.HTTPStatusCode
			}

			var tempEvent models.AxisTransformationEvent
			tempEvent.ID = event.ID
			tempEvent.MessageID = event.MessageID
			tempEvent.Headers = headers
			tempEvent.Payload = payload
			tempEvent.EventCode = event.EventCode
			tempEvent.CreatedDate = event.CreatedDate
			tempEvent.HTTPStatusCode = HTTPStatusCode

			AxisTransformationEvents = append(AxisTransformationEvents, tempEvent)
		}

		var totalItems int64
		err = database.WrapGet(dbconnections.AXISConnectionPool, ctx, &totalItems, "GetAxisTransformationEventsSize")
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapGet error")
		}

		totalPages := totalItems / limit

		templateDataMap["DataMap"] = DataMap
		templateDataMap["ItemsPerPage"] = limit
		templateDataMap["PageNumber"] = page
		templateDataMap["TotalItems"] = totalItems
		templateDataMap["TotalPages"] = totalPages

		templateDataMap["AxisTransformationEvents"] = AxisTransformationEvents

		return c.Render(http.StatusOK, "transformationevents", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}
