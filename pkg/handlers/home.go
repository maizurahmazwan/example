package handlers

import (
	"math/rand"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/env"
)

func shuffle(arr []string) {
	t := time.Now()
	rand.Seed(int64(t.Nanosecond())) // no shuffling without this line

	for i := len(arr) - 1; i > 0; i-- {
		j := rand.Intn(i)
		arr[i], arr[j] = arr[j], arr[i]
	}
}

// Home show login prompt, but process username/password at Login func
func Home(c echo.Context) error {

	ImageList := []string{"backoffice-home-0.jpg",
		"backoffice-home-1.jpg",
		"backoffice-home-2.jpg",
		"backoffice-home-3.jpg",
		"backoffice-home-4.jpg",
		"backoffice-home-5.jpg",
		"backoffice-home-6.jpg",
		"backoffice-home-7.jpg",
		"backoffice-home-8.jpg",
		"backoffice-home-9.jpg",
		"backoffice-home-10.jpg",
		"backoffice-home-11.jpg",
		"backoffice-home-12.jpg",
		"backoffice-home-13.jpg",
		"backoffice-home-14.jpg",
		"backoffice-home-15.jpg",
		"backoffice-home-16.jpg",
		"backoffice-home-17.jpg",
		"backoffice-home-18.jpg",
		"backoffice-home-19.jpg",
		"backoffice-home-20.jpg",
		"backoffice-home-21.jpg",
		"backoffice-home-22.jpg",
		"backoffice-home-23.jpg",
		"backoffice-home-24.jpg",
		"backoffice-home-25.jpg",
		"backoffice-home-26.jpg",
		"backoffice-home-27.jpg",
		"backoffice-home-28.jpg",
		"backoffice-home-29.jpg",
		"backoffice-home-30.jpg",
		"backoffice-home-31.jpg",
		"backoffice-home-32.jpg",
		"backoffice-home-33.jpg",
		"backoffice-home-34.jpg",
		"backoffice-home-35.jpg",
		"backoffice-home-36.jpg",
		"backoffice-home-37.jpg",
		"backoffice-home-38.jpg",
		"backoffice-home-39.jpg",
		"backoffice-home-40.jpg",
		"backoffice-home-41.jpg",
		"backoffice-home-42.jpg",
		"backoffice-home-43.jpg",
		"backoffice-home-44.jpg",
		"backoffice-home-45.jpg",
		"backoffice-home-46.jpg",
		"backoffice-home-47.jpg",
		"backoffice-home-48.jpg",
		"backoffice-home-49.jpg",
		"backoffice-home-50.jpg",
		"backoffice-home-51.jpg",
		"backoffice-home-52.jpg",
		"backoffice-home-53.jpg",
	}

	shuffle(ImageList) // randomize background image
	variablesMap := map[string]interface{}{}
	variablesMap["ImageFilename"] = ImageList[1] // pick 1
	variablesMap["MDMPath"] = env.Get("MDM_PATH")

	return c.Render(http.StatusOK, "homeisloginpage", variablesMap)

}
