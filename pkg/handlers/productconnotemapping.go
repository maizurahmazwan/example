package handlers

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"pbo-server/internal/models"
	"pbo-server/pkg/dbconnections"
	"pbo-server/pkg/http/responses"
	"pbo-server/pkg/utilities"
	"strconv"
	"strings"

	"github.com/go-errors/errors"
	"github.com/golang-jwt/jwt"
	"github.com/jackc/pgx/v4"
	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/contextkeys"
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"
)

func ProductConnoteMapping(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		// extract data from product connote mapping with pagination

		// Get page number from path `productconnotemapping/:page`
		pageNumber := c.Param("page")
		page, err := strconv.Atoi(pageNumber)
		if err != nil || page < 1 {
			page = 1
		}

		// set pageSize to 10 for now
		pageSize := 10

		offset := (page - 1) * pageSize

		// Get search query from form input
		search := c.FormValue("search")

		// get the rows paginated by offset
		var rows pgx.Rows
		var totalItems int

		if c.Request().Method == "POST" && search != "" {

			if search != "" {
				// search for items that match the query
				query := fmt.Sprintf("SELECT * FROM product_connote_mapping WHERE prefix LIKE '%%%s%%' OR usage LIKE '%%%s%%' ORDER BY created_at DESC LIMIT $1 OFFSET $2", search, search)
				rows, err = database.WrapQuery(dbconnections.MDMDBPool, ctx, query, pageSize, offset)
				if err != nil {
					loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapQuery error")
				}
			}
		} else {

			// get all items
			rows, err = database.WrapQuery(dbconnections.MDMDBPool, ctx, "GetFromProductConnoteMappingPaginatedByOffset", pageSize, offset)
			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapQuery error")
			}

			// Get the total item count of product connote mappings
			err = database.WrapQueryRow(dbconnections.MDMDBPool, ctx, "GetTotalSizeFromProductConnoteMapping").Scan(&totalItems)
			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapQueryRow error")
			}
		}

		results := make([]models.ProductConnoteMapping, 0)

		for rows.Next() {
			var data models.ProductConnoteMapping
			err := rows.Scan(&data.Sid, &data.Prefix, &data.Suffix, &data.Usage, &data.Service_segmentation, &data.Product_owner, &data.Description, &data.Customer_type, &data.Remark, &data.CreatedAt, &data.UpdatedAt)
			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapQuery error")
			}
			results = append(results, data)
		}

		totalPages := totalItems / pageSize
		nextPage := page + 1
		prevPage := page - 1

		templateDataMap["DataMap"] = DataMap
		templateDataMap["ProductConnoteMapping"] = results
		templateDataMap["Page"] = page
		templateDataMap["PageSize"] = pageSize
		templateDataMap["TotalItems"] = totalItems
		templateDataMap["PageItemCount"] = pageSize
		templateDataMap["TotalPages"] = totalPages
		templateDataMap["NextPage"] = nextPage
		templateDataMap["PrevPage"] = prevPage
		templateDataMap["Search"] = search

		return c.Render(http.StatusOK, "productconnotemapping", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}
}

func UploadProductConnoteMapping(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	if c.Request().Method == "POST" {
		file, _, err := c.Request().FormFile("csvFile")

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error getting uploaded file")
			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "productconnotemapping", templateDataMap)

		}

		// validate the uploaded file
		reader := csv.NewReader(file)
		reader.Comma = ','
		reader.Comment = '#'
		reader.LazyQuotes = true
		recordsFromCSV, err := reader.ReadAll()
		if len(recordsFromCSV) == 0 || err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error opening uploaded csv file")

			DataMap["UploadError"] = true
			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "productconnotemapping", templateDataMap)

		}

		// remove first row if it's header
		if strings.EqualFold(recordsFromCSV[0][1], "prefix") && strings.EqualFold(recordsFromCSV[0][2], "suffix") {
			recordsFromCSV = recordsFromCSV[1:]
		}

		// // temporarily store postcode range for checking
		// m := make(map[int]int)

		// sanity check on each column
		for _, record := range recordsFromCSV {
			data := models.ProductConnoteMapping{
				Prefix:               record[0],
				Suffix:               record[1],
				Usage:                record[2],
				Service_segmentation: record[3],
				Product_owner:        record[4],
				Description:          record[5],
				Customer_type:        record[6],
				Remark:               record[7],
			}

			// make sure all fields are not empty
			if data.Prefix == "" {
				err := errors.New("missing prefix in a row")
				loggerWithTrace.Error().Err(err).Caller().Msg("error in csv content")

				DataMap["UploadError"] = true
				DataMap["Message"] = err.Error()
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "productconnotemapping", templateDataMap)
			}
		}

		// load all rows from database
		var productconnotemappingFromDB []models.ProductConnoteMapping

		errDB := database.WrapSelect(dbconnections.MDMDBPool, ctx, &productconnotemappingFromDB, "GetAllFromProductConnoteMapping")
		if errDB != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapSelect error")
			DataMap["UploadError"] = true
			DataMap["Message"] = errDB
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "productconnotemapping", templateDataMap)
		}

		// insert - find additional row inside csv
		var insertFromCSV []models.ProductConnoteMapping

		for _, record := range recordsFromCSV {
			data := models.ProductConnoteMapping{
				Prefix:               strings.TrimSpace(record[0]),
				Suffix:               strings.TrimSpace(record[1]),
				Usage:                strings.TrimSpace(record[2]),
				Service_segmentation: strings.TrimSpace(record[3]),
				Product_owner:        strings.TrimSpace(record[4]),
				Description:          strings.TrimSpace(record[5]),
				Customer_type:        strings.TrimSpace(record[6]),
				Remark:               strings.TrimSpace(record[7]),
			}

			if !strings.EqualFold(data.Prefix, "prefix") && !strings.EqualFold(data.Suffix, "suffix") {
				insertFromCSV = append(insertFromCSV, data)
			}

		}

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true
		DataMap["InsertCodes"] = insertFromCSV
		DataMap["ExitingRowsCount"] = len(productconnotemappingFromDB)
		DataMap["NewRowsCount"] = len(insertFromCSV)

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "updateproductconnotemapping", templateDataMap)

	}

	// user did not select a file and click submit button.
	return echo.ErrBadRequest
	//return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH")+"/hscodes")
}

func UpdateProductConnoteMapping(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)

	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		err := c.Request().ParseForm()

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error parsing form")
		}

	}

	if c.Request().Method == "POST" {

		var dataToInsert []models.ProductConnoteMapping
		var data models.ProductConnoteMapping

		for formKey, inputValues := range c.Request().Form {
			if formKey == "productconnotemappingValue[]" {

				for _, value := range inputValues {
					row := strings.Split(value, ",")

					data.Prefix = row[0]
					data.Suffix = row[1]
					data.Usage = row[2]
					data.Service_segmentation = row[3]
					data.Product_owner = row[4]
					data.Description = row[5]
					data.Customer_type = row[6]
					data.Remark = row[7]

					dataToInsert = append(dataToInsert, data)
				}
			}
		}

		_, err := database.WrapExec(dbconnections.MDMDBPool, ctx, "DeleteAllFromProductConnoteMapping")

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error in deleting all in product_connote_mapping table")

			DataMap["UploadSuccess"] = false
			DataMap["UploadError"] = true

			DataMap["Message"] = err.Error()
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "productconnotemapping", templateDataMap)

		} else {
			// log activity
			utilities.InsertActivity(c, "UpdateProductConnoteMapping: DeleteAllFromProductConnoteMapping", DataMap["Username"].(string), "product_connote_mapping")

			// proceed with insert after successful delete
			rowsToInsert := [][]interface{}{}

			for i := 0; i < len(dataToInsert); i++ {
				row := []interface{}{
					dataToInsert[i].Prefix,
					dataToInsert[i].Suffix,
					dataToInsert[i].Usage,
					dataToInsert[i].Service_segmentation,
					dataToInsert[i].Product_owner,
					dataToInsert[i].Description,
					dataToInsert[i].Customer_type,
					dataToInsert[i].Remark,
				}
				rowsToInsert = append(rowsToInsert, row)
			}

			copyCount, err := database.WrapCopyFrom(dbconnections.MDMDBPool, ctx, pgx.Identifier{"product_connote_mapping"},
				[]string{"prefix", "suffix", "usage", "service_segmentation", "product_owner", "description", "customer_type", "remark"},
				pgx.CopyFromRows(rowsToInsert))

			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in insert new records to product_connote_mapping table")
				DataMap["UploadSuccess"] = false
				DataMap["UploadError"] = true
				DataMap["Message"] = err.Error()
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "productconnotemapping", templateDataMap)
			}

			if int(copyCount) != len(dataToInsert) {
				loggerWithTrace.Info().Msgf("Expected CopyFrom to return %d copied rows, but got %d", len(rowsToInsert), copyCount)

				DataMap["UploadSuccess"] = false
				DataMap["UploadError"] = true

				DataMap["Message"] = "The number rows to insert not equal to the number of copied rows."
				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "productconnotemapping", templateDataMap)
			} else {
				// log activity
				utilities.InsertActivity(c, "UpdateProductConnotemapping: InsertAllIntoProductConnoteMapping", DataMap["Username"].(string), "product_connote_mapping")

				copyCountString := fmt.Sprintf("%d", copyCount)
				DataMap["Message"] = copyCountString + " new records inserted."
				DataMap["UploadSuccess"] = true
				DataMap["UploadError"] = false

				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "productconnotemapping", templateDataMap)
			}

		}

	}

	// user token has expired, return the user back to home page
	return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))

}
