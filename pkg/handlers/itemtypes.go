package handlers

import (
	"fmt"
	"net/http"
	"pbo-server/internal/models"
	"pbo-server/pkg/dbconnections"
	"pbo-server/pkg/http/responses"
	"pbo-server/pkg/utilities"
	"strconv"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/pos_malaysia/golib/contextkeys"
	"gitlab.com/pos_malaysia/golib/database"
	"gitlab.com/pos_malaysia/golib/env"
	"gitlab.com/pos_malaysia/golib/logs"
)

func ItemTypes(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		// extract data from item_types table
		var itemTypes = []models.ItemTypes{}

		rows, err := database.WrapQuery(dbconnections.MDMDBPool, ctx, "GetAllFromItemTypes")
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapQuery error")
		}

		for rows.Next() {
			itemType := models.ItemTypes{}

			if err := rows.Scan(&itemType.SID, &itemType.Name, &itemType.Value, &itemType.CreatedAt, &itemType.UpdatedAt); err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapQuery error")
			}

			itemTypes = append(itemTypes, itemType)
		}

		rows.Close()

		templateDataMap["DataMap"] = DataMap
		templateDataMap["ItemTypes"] = itemTypes

		return c.Render(http.StatusOK, "itemtypes", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}

func AddItemType(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "additemtype", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}

func AddedItemType(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

	} else {

		DataMap["LoginActive"] = false
		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))

	}

	if c.Request().Method == "POST" {
		name := c.Request().FormValue("itemname")
		if name == "" {
			loggerWithTrace.Error().Err(err).Caller().Msg("error in name field")
			DataMap["AddError"] = true
			DataMap["Message"] = "Name cannot empty."
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "additemtype", templateDataMap)
		}

		value := c.Request().FormValue("itemvalue")
		if value == "" {
			loggerWithTrace.Error().Err(err).Caller().Msg("error in value field")
			DataMap["AddError"] = true
			DataMap["Message"] = "Value cannot empty."
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "additemtype", templateDataMap)
		}

		// sanity check on value input
		if _, err := strconv.ParseInt(value, 10, 32); err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("error in value field")
			DataMap["AddError"] = true
			DataMap["Message"] = "Value is not a number."
			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "additemtype", templateDataMap)
		}

		// add new item type into database
		_, err := database.WrapExec(dbconnections.MDMDBPool, ctx, "InsertIntoItemTypes", name, value)

		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapExec error")
		}

		// log activity
		utilities.InsertActivity(c, "AddedItemType: InsertIntoItemTypes item ["+name+","+value+"]", DataMap["Username"].(string), "item_types")

		DataMap["AddSuccess"] = true
		DataMap["Message"] = "Add new item type successful."
		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "additemtype", templateDataMap)

	}

	// user did not fill name or value field
	return echo.ErrBadRequest

}

func EditItemType(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		// get the SID from query parameter
		sid := c.Param("sid")

		// extract data associated with this sid from item_types table
		var itemTypeToEdit = models.ItemTypes{}
		err := database.WrapGet(dbconnections.MDMDBPool, ctx, &itemTypeToEdit, "GetFromItemTypesBySID", sid)
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapGet error")
			DataMap["EditError"] = true
			DataMap["Message"] = err
		}

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true
		DataMap["SID"] = itemTypeToEdit.SID
		DataMap["Name"] = itemTypeToEdit.Name
		DataMap["Value"] = itemTypeToEdit.Value

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "edititemtype", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}

func UpdateItemType(c echo.Context) error {

	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	uriSegments := strings.Split(c.Request().URL.String(), "/")
	DataMap["CurrentPage"] = uriSegments[2]

	tokenCookie, err := c.Cookie(CookieName)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)

	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	var templateDataMap = make(map[string]interface{})

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true

		if c.Request().Method == "POST" {
			itemName := c.Request().FormValue("itemName")
			itemValue := c.Request().FormValue("itemValue")
			itemSID := c.Request().FormValue("itemSID")

			if itemName == "" || itemValue == "" {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in name field")
				DataMap["EditError"] = true
				DataMap["Message"] = "Name cannot empty."
				DataMap["SID"] = itemSID
				DataMap["itemName"] = itemName   // populate back the name input by user
				DataMap["itemValue"] = itemValue // populate back the value input by user

				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "edititemtype", templateDataMap)
			}

			// sanity check on value input
			if _, err := strconv.ParseInt(itemValue, 10, 32); err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in value field")
				DataMap["EditError"] = true
				DataMap["Message"] = "Value is not a number."
				DataMap["SID"] = itemSID
				DataMap["itemName"] = itemName   // populate back the name input by user
				DataMap["itemValue"] = itemValue // populate back the value input by user

				templateDataMap["DataMap"] = DataMap

				return c.Render(http.StatusOK, "edititemtype", templateDataMap)
			}

			commandTag, err := database.WrapExec(dbconnections.MDMDBPool, ctx, "UpdateItemTypesBySID", itemName, itemValue, itemSID)

			if err != nil {
				loggerWithTrace.Error().Err(err).Caller().Msg("error in updating item type")
				// instead of return unauthorized error, return the user back to home page
				return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
			}

			// log activity
			utilities.InsertActivity(c, "UpdateItemType: UpdateItemTypesBySID SID ["+itemSID+"]", DataMap["Username"].(string), "item_types")

			rowsUpdated := fmt.Sprintf("%d", commandTag.RowsAffected())
			loggerWithTrace.Info().Str("rows updated", rowsUpdated).Send()

			DataMap["EditSuccess"] = true
			DataMap["Message"] = "Updated SID : [" + itemSID + "]'s values in item_types table"

			templateDataMap["DataMap"] = DataMap

			return c.Render(http.StatusOK, "updateitemtype", templateDataMap)

		}

		templateDataMap["DataMap"] = DataMap

		return c.Render(http.StatusOK, "additemtype", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}

func DelItemType(c echo.Context) error {
	//Get client request ID from header if available
	pRequestID := c.Request().Header.Get(responses.ClientRequestID)
	serverTraceID := c.Response().Header().Get(echo.HeaderXRequestID)

	// pass client request ID and Echo's trace ID to child context
	ctx := c.Request().Context()
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_CLIENT_REQUEST_ID, pRequestID)
	ctx = contextkeys.SetContextValue(ctx, contextkeys.CONTEXT_KEY_SERVER_TRACE_ID, serverTraceID)

	loggerWithTrace := logs.With().Str("P-Request-Id", pRequestID).Str("server_trace_id", serverTraceID).Logger()

	tokenCookie, err := c.Cookie(CookieName)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	tokenString := tokenCookie.Value
	if tokenString == "" {
		loggerWithTrace.Error().Err(err).Caller().Msg("error getting token cookie")

		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	token, err := utilities.ExtractToken(tokenString)
	if err != nil {
		loggerWithTrace.Error().Err(err).Caller().Msg("error parsing token cookie")
		// instead of return unauthorized error, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

	DataMap := echo.Map{}
	DataMap["MDMPath"] = env.Get("MDM_PATH")

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		// get the SID from query parameter
		sid := c.Param("sid")
		if sid == "" {
			loggerWithTrace.Error().Err(err).Caller().Msg("invalid sid error")
			DataMap["DelError"] = true
			DataMap["Message"] = err
		}

		// delete item type from database
		commandTag, err := database.WrapExec(dbconnections.MDMDBPool, ctx, "DeleteFromItemTypes", sid)
		if err != nil {
			loggerWithTrace.Error().Err(err).Caller().Msg("database.WrapExec error")
			DataMap["EditError"] = true
			DataMap["Message"] = err
		}

		DataMap["Email"] = claims["email"].(string)
		DataMap["Username"] = claims["username"].(string)
		DataMap["Avatar"] = claims["avatar"].(string)
		DataMap["LoginActive"] = true
		DataMap["SID"] = sid

		templateDataMap["DataMap"] = DataMap

		// log activity
		utilities.InsertActivity(c, "DelItemType: DeleteFromItemTypes SID : ["+sid+"]", DataMap["Username"].(string), "item_types")

		rowsUpdated := fmt.Sprintf("%d", commandTag.RowsAffected())
		loggerWithTrace.Info().Str("rows updated", rowsUpdated).Send()

		DataMap["DelSuccess"] = true
		DataMap["Message"] = "Deleted SID : [" + sid + "] in item_types table"

		return c.Render(http.StatusOK, "delitemtype", templateDataMap)
	} else {

		// user token has expired, return the user back to home page
		return c.Redirect(http.StatusMovedPermanently, env.Get("MDM_PATH"))
	}

}
