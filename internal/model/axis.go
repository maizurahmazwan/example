package models

import (
	"time"

	"github.com/gofrs/uuid"
)

type (
	AxisTransformationEventFromDB struct {
		ID             uuid.UUID `db:"id"`
		MessageID      string    `db:"message_id"`
		Headers        *string   `db:"headers"`
		Payload        *string   `db:"payload"`
		EventCode      string    `db:"event_code"`
		CreatedDate    time.Time `db:"created_date"`
		HTTPStatusCode *string   `db:"http_status_code"`
	}

	AxisTransformationEvent struct {
		ID             uuid.UUID
		MessageID      string
		Headers        string
		Payload        string
		EventCode      string
		CreatedDate    time.Time
		HTTPStatusCode string
	}
)
