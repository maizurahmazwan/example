package models

import (
	"fmt"
	"time"
)

type (
	DashBoard struct {
		Sid        int       `json:"sid" db:"sid"`
		Action     string    `json:"action" db:"action"`
		By_who     string    `json:"by_who" db:"by_who"`
		Table_name string    `json:"table_name" db:"table_name"`
		Created_at time.Time `json:"created_at" db:"created_at"`
		Updated_at time.Time `json:"updated_at" db:"updated_at"`
	}
)

func (a DashBoard) String() string {
	return fmt.Sprintf("[%s,%s,%s,%s]", a.Sid, a.Action, a.By_who, a.Table_name)
}
