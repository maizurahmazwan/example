package models

import (
	"fmt"
	"time"
)

type (
	Postcodes struct {
		Sid          int       `json:"sid" db:"sid"`
		Country      string    `json:"country" db:"country"`
		PostcodeFrom string    `json:"postcode_from" db:"postcode_from"`
		PostcodeTo   string    `json:"postcode_to" db:"postcode_to"`
		CityName     string    `json:"city_name" db:"city_name"`
		StateName    string    `json:"state_name" db:"state_name"`
		StateCode    string    `json:"state_code" db:"state_code"`
		CreatedAt    time.Time `json:"created_at" db:"created_at"`
		UpdatedAt    time.Time `json:"updated_at" db:"updated_at"`
	}
)

func (p Postcodes) String() string {
	return fmt.Sprintf("[%s,%s,%s,%s]", p.PostcodeFrom, p.PostcodeTo, p.CityName, p.StateName)
}
