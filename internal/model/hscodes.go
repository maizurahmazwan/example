package models

import (
	"fmt"
	"time"
)

type (
	HScodes struct {
		Sid       int       `json:"sid" db:"sid"`
		HScode    string    `json:"hscode" db:"hscode"`
		Keyword   string    `json:"keyword" db:"keyword"`
		CreatedAt time.Time `json:"created_at" db:"created_at"`
		UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
	}
)

func (h HScodes) String() string {
	return fmt.Sprintf("[%s,%s]", h.HScode, h.Keyword)
}
