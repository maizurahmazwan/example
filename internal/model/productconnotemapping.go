package models

import (
	"fmt"
	"time"
)

type (
	ProductConnoteMapping struct {
		Sid                  int       `json:"sid" db:"sid"`
		Prefix               string    `json:"prefix" db:"prefix"`
		Suffix               string    `json:"suffix" db:"suffix"`
		Usage                string    `json:"usage" db:"usage"`
		Service_segmentation string    `json:"service_segmentation" db:"service_segmentation"`
		Product_owner        string    `json:"product_owner" db:"product_owner"`
		Description          string    `json:"description" db:"description"`
		Customer_type        string    `json:"customer_type" db:"customer_type"`
		Remark               string    `json:"remark" db:"remark"`
		CreatedAt            time.Time `json:"created_at" db:"created_at"`
		UpdatedAt            time.Time `json:"updated_at" db:"updated_at"`
	}
)

func (c ProductConnoteMapping) String() string {
	return fmt.Sprintf("[%s,%s,%s,%s,%s,%s,%s,%s]", c.Prefix, c.Suffix, c.Usage, c.Service_segmentation, c.Product_owner, c.Description, c.Customer_type, c.Remark)
}
